const {Schema, model} = require('mongoose');

const movieSchema = new Schema ({
    title: String,
    year: Number,
    released: String,
    genre: String,
    director: String, 
    actors: String,
    plot: String,
    ratings: [{source: String, value: String}]
});

module.exports = model('moviesnode',movieSchema,'moviesnode');