# Challenge de CLM Consultures
## _API Rest de películas_

## Puntos realizados del challenge

- Existen 3 endpoints uno que lista ('/'), otro que ingresa ('/add') y finalmente uno que actualiza ('/update')
- GET('/') => Nos lista todas las películas que se encuentran en la base de datos
- POST('/add') => Nos agrega una película obtenida en la API entregada a la BD. Requiere de un parámetro llamado "search"
- POST('/update') => Nos actualiza el PLOT del registro indicado por parámetro. Requiere de 3 parámetros que son "titulo", "texto" (texto a buscar en PLOT) y "cambio" (texto por el que cambiaremos lo encontrado anteriormente)

## Instalación

Se debe descargar el repositorio en su computadora, para eso es necesario tener instalado git (https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
Para funcionar en ambiente local es necesario [Node.js](https://nodejs.org/) v10+.
Además se debe instalar el motor de MongoDB (https://docs.mongodb.com/manual/installation/) para poder utilizar la aplicación. Esto conlleva modificar el archivo app.js en la línea 43 en donde cambiaremos esto:
```sh
const connection = mongoose.connect('mongodb://db/movies_challenge',{useNewUrlParser: true, useUnifiedTopology: true})
```
Por esto:
```sh
const connection = mongoose.connect('mongodb://localhost:27017/movies_challenge',{useNewUrlParser: true, useUnifiedTopology: true})
```
Para así poder continuar con la base de datos local de MongoDB.

Posterior a eso se deben instalar las dependencias.

```sh
git clone https://gitlab.com/fgaete/challenge_clm.git
cd challenge_clm
npm install
npm start
```
## Docker

La aplicación también se puede utilizar con Docker puesto que en el proyecto viene el respectivo Dockerfile y docker-compose.yml
Para esto previamente se requiere tener instalado Docker (https://docs.docker.com/get-docker/) en sus equipos, así también Docker Compose (https://docs.docker.com/compose/install/) para ejecutar el archivo YAML

```sh
cd challenge_clm
docker-compose up -d
```

Esto va a crear dos contenedores, uno correspondiente a la base de datos en MongoDB y otra correspondiente a la aplicación

Pueden verificar que todo se encuentra funcionando con la siguiente URL en su navegador favorito 

```sh
127.0.0.1:3200
```