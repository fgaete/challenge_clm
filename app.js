// Importaciones
const Koa = require('koa');
const Router = require('koa-router');
const bodyParser = require('koa-bodyparser');
const request = require('request');
const mongoose = require('mongoose');
const Movies = require('./models/movie');

// Se inicializan constantes y variables
const app = new Koa();
const router = new Router();
const port = 3000;
const apikey = '54f94c94';
const url = 'http://www.omdbapi.com/?apikey=';

const connection = mongoose.connect('mongodb://db/moviesnode',{useNewUrlParser: true, useUnifiedTopology: true})
        .then(()=>{
            console.log("Conectado a BD correctamente!");
            
        })
        .catch(err=> console.log(err));
//Middleware de BodyParser
app.use(bodyParser());

//Lista de películas insertadas
var listaID = [];

//Rutas
router.get('/',index);
router.post('/add',add);
router.post('/replace',replace);

//Function que obtiene todas las películas almacenadas en la BD
async function index(ctx){
    //Console log para indicar donde nos encontramos
    console.log('We are on index');
    await connection;
    return new Promise((resolve, reject) => {
    const lista = Movies.find({});
        lista.exec((er, res) => {
            if (er) {  
                reject(er);   
            }
            else {
                resolve(res);
                console.log(res);
                ctx.body = res;
            }
        });
    });
}

//Function que busca una película en API OMDB y la inserta en BD
async function add(ctx){
    console.log('We are on add');
    var param = ctx.request.query.search;
    if(param){
        await connection;
        request(url+apikey+'&t='+param, { json: true }, (err, res, body) => {
            if (err) { return console.log(err); }
            //Va a buscar la película a la BD
            return new Promise((resolve, reject) => {
                var found = Movies.find({'title':body.Title});
                found.exec((er, res) => {
                    if (er) {  
                        reject(er);   
                    }
                    else { 
                        //Si la película no se encuentra repetida inserta, sino indica que es una película repetida
                        if (!res.length){
                            var movies = new Movies({
                                title: body.Title,
                                year: body.Year,
                                released: body.Released,
                                genre: body.Genre,
                                director: body.Director,
                                actors: body.Actors,
                                plot: body.Plot,
                                ratings: body.Ratings
                            });
                            movies.save(function(err,res){
                                if (err) return console.error(err);
                                console.log(body.Title + '- ingresada con éxito');
                                resolve(res);
                                ctx.body = body.Title + '- ingresada con éxito';
                            })
                            
                        } else {
                            console.log('Película repetida');
                        }
                    }
                });  
            });
        });
    } else {
        console.log('No se ingresó nada para buscar')
    }
}

//Function que busca una película en BD y posteriormente reemplaza texto en plot
async function replace(ctx){
    console.log('We are on replace');
    var param = ctx.request.query.titulo;    
    var texto = ctx.request.query.texto;
    var cambio = ctx.request.query.cambio;
    if ((param) && (texto) && (cambio)){
        await connection;
        return new Promise((resolve, reject) => {
            var found = Movies.find({'title':param});
            found.exec((er, res) => {
                if (er) {  
                    reject(er);   
                }
                else { 
                    res.forEach(busqueda=>{
                        let plot = busqueda.plot;
                        cambiado = plot.replace(texto,cambio);
                        busqueda.plot = cambiado;
                        Movies.findByIdAndUpdate(busqueda._id,busqueda,{new:true, runValidators:true}).exec((er,res) => {
                            if (er) {  
                                reject(er);   
                            }
                            else {
                                resolve(res);
                                ctx.body = res.title + ' - Película actualizada';
                            }    
                        })
                    })
                }
            });
        })
    } else {
        console.log('No están los parámetros suficientes para esta función');
    }
}


//Configuración de Middleware de rutas
app.use(router.routes()).
    use(router.allowedMethods());

app.listen(port,()=> console.log('Servicio iniciado con éxito'));